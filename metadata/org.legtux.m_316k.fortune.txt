Categories:Reading
License:MIT
Web Site:
Source Code:https://github.com/316k/android-fortune
Issue Tracker:https://github.com/316k/android-fortune/issues

Auto Name:Fortunes
Summary:View Unix fortunes
Description:
Simple viewer for [https://en.wikipedia.org/wiki/Fortune_%28Unix%29 Unix fortunes].
.

Repo Type:git
Repo:https://github.com/316k/android-fortune

Build:1.0,1
    commit=v1.0
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0
Current Version Code:1

