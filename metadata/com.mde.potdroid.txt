Categories:Internet,Reading
License:GPLv2
Web Site:https://projects.oelerich.org/projects/pot-droid
Source Code:https://projects.oelerich.org/projects/pot-droid/repository
Issue Tracker:https://projects.oelerich.org/projects/pot-droid/issues

Auto Name:pOT-Droid
Summary:Companion app for mods.de forum
Description:
Companion app for the german [http://forum.mods.de/bb mods.de] forum.

[https://projects.oelerich.org/projects/pot-droid/repository/revisions/master/entry/CHANGELOG.md Changelog]
.

Repo Type:git
Repo:https://github.com/janoliver/pOT-Droid

Build:4.0.8,57
    commit=4.0.8
    gradle=yes
    srclibs=PreferenceFragment@9eac3cdb34a8dad96d9d42b716489d1b27143326
    prebuild=sed -i -e '/maven {/,+2d' -e '/preferencefragment/d' -e "/appcompat-v7/acompile project(':preffragment')" build.gradle && \
        echo -e "include 'preffragment'\nproject(':preffragment').projectDir = new File('$$PreferenceFragment$$')" >> settings.gradle

Build:4.0.9,58
    commit=4.0.9
    gradle=yes
    srclibs=PreferenceFragment@9eac3cdb34a8dad96d9d42b716489d1b27143326
    prebuild=sed -i -e '/maven {/,+2d' -e '/preferencefragment/d' -e "/appcompat-v7/acompile project(':preffragment')" build.gradle && \
        echo -e "include 'preffragment'\nproject(':preffragment').projectDir = new File('$$PreferenceFragment$$')" >> settings.gradle

Build:4.0.10,59
    commit=4.0.10
    gradle=yes
    srclibs=PreferenceFragment@9eac3cdb34a8dad96d9d42b716489d1b27143326
    prebuild=sed -i -e '/maven {/,+2d' -e '/preferencefragment/d' -e "/appcompat-v7/acompile project(':preffragment')" build.gradle && \
        echo -e "include 'preffragment'\nproject(':preffragment').projectDir = new File('$$PreferenceFragment$$')" >> settings.gradle

Build:4.1.1,61
    commit=4.1.1
    gradle=yes
    srclibs=PreferenceFragment@9eac3cdb34a8dad96d9d42b716489d1b27143326
    prebuild=sed -i -e '/maven {/,+2d' -e '/preferencefragment/d' -e "/appcompat-v7/acompile project(':preffragment')" build.gradle && \
        echo -e "include 'preffragment'\nproject(':preffragment').projectDir = new File('$$PreferenceFragment$$')" >> settings.gradle

Auto Update Mode:None
Update Check Mode:Tags
Current Version:4.1.1
Current Version Code:61

